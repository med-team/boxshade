boxshade (3.3.1-14) unstable; urgency=medium

  * Standards-Version: 4.5.1 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 07 Dec 2020 21:08:29 +0100

boxshade (3.3.1-13) unstable; urgency=medium

  [ Nguyen Hoang Tung ]
  * Build for correct architecture
    Closes: #931150

  [ Andreas Tille ]
  * Standards-Version: 4.3.0

 -- Andreas Tille <tille@debian.org>  Thu, 27 Jun 2019 13:54:24 +0200

boxshade (3.3.1-12) unstable; urgency=medium

  [ Steffen Moeller ]
  * debian/upstream/metadata: Added references to registries.

  [ Andreas Tille ]
  * Use fake watch file since other locations are outdated or vanished
  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Fri, 19 Oct 2018 14:58:50 +0200

boxshade (3.3.1-11) unstable; urgency=medium

  * Moved packaging from SVN to Git
  * Standards-Version: 4.1.1

 -- Andreas Tille <tille@debian.org>  Mon, 13 Nov 2017 21:28:29 +0100

boxshade (3.3.1-10) unstable; urgency=medium

  * d/watch: ftpserver vanished, find other location to silence uscan
  * debhelper 10
  * d/copyright: update
  * cme fix dpkg-control
  * Fix spelling
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Wed, 18 Jan 2017 09:30:23 +0100

boxshade (3.3.1-9) unstable; urgency=medium

  * Update README.Debian
  * DEP3 header
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Mon, 01 Feb 2016 09:10:50 +0100

boxshade (3.3.1-8) unstable; urgency=low

  * debian/patches/remove-newlines.patch: restores interactive use.
    Thanks: Giorgio Maccari. Closes: #700088.
  * debian/control:
    - normalised with 'cme fix dpkg-control';
    - removed obsolete DM-Upload-Allowed field;
    - normalised VCS URLs.
  * Added example data and output, and basic regression tests.
  * Machine-readable Debian copyright file.
  * Use Debhelper 9, verbosely.
  * Pass dpkg build flags through debian/rules.
  * Removed obsolete instructions about the quilt patch system.
  * Added command line parameters to manual page.
  * Conforms to Policy 3.9.4.

 -- Charles Plessy <plessy@debian.org>  Mon, 23 Sep 2013 10:58:52 +0900

boxshade (3.3.1-7) unstable; urgency=low

  * debian/install: Make sure boxshade executable will be installed
    in the proper location
    Closes: #683375 (LP: #1017188)

 -- Andreas Tille <tille@debian.org>  Tue, 31 Jul 2012 11:17:22 +0200

boxshade (3.3.1-6) unstable; urgency=low

  [ Mathieu Malaterre ]
  * debian/get-orig-source: Add GZIP="--no-name" option to enable identical
    tarballs

  [ A. Costa ]
  * Corrected spelling in debian/boxshade.1.xml.  Closes: #650473.

  [ Andreas Tille ]
  * debian/control:
    - Standards-Version: 3.9.2 (no changes needed)
    - Fixed Vcs fields
  * Debhelper 8 (control+compat)
  * debian/bin/boxshade
    - moved debian/boxshade.sh to this location to enable easier
      usage of dh_install
    - s#/bin/bash#/bin/sh#
  * debian/install
    - make use of dh_install
  * debian/manpages
  * debian/rules: Rewritten do use dh to profit easily from general
    changes

 -- Andreas Tille <tille@debian.org>  Sat, 07 Jan 2012 09:00:33 +0100

boxshade (3.3.1-5) unstable; urgency=low

  [ David Paleino ]
  * Removed myself from Uploaders

  [ Andreas Tille ]
  * debian/control
    - Standards-Version: 3.9.1 (no changes needed)
    - Debhelper 7
    - Fix spelling of Debian Med in maintainer address
  * debian/rules: s/dh_clean -k/dh_prep/
  * debian/source/format: 3.0 (quilt)

 -- Andreas Tille <tille@debian.org>  Fri, 14 Jan 2011 08:27:01 +0100

boxshade (3.3.1-4) unstable; urgency=low

  [ Charles Plessy ]

  * Updated my email address.

  [ Steffen Moeller ]

  * Eliminated use of gets from source
  * boxshade bails out with an error if there is an error and does not
    expect sudden user input ... it is scriptable now.

 -- Steffen Moeller <moeller@debian.org>  Sun, 27 Apr 2008 16:56:02 +0900

boxshade (3.3.1-3) unstable; urgency=low

  [ David Paleino ]
  * debian/boxshade.1 has been statically generated: this saves some
    time (and space) on buildds (leaving the XML version for future
    editing)
  * debian/control:
    - B-D updated (manpage is not generated on buildds anymore)
    - added myself to Uploaders
    - XS-Vcs-* moved to Vcs-* and fixed
    - Homepage field now replaces the old pseudo-field in the long
      description
    - removed XS- from DM-Upload-Allowed
  * debian/rules:
    - reflecting static generation of boxshade.1 -- removing boxshade.1
      target
    - some general cleanup
  * debian/README.Debian updated: the new debian-med.alioth.d.o website
    is now mentioned.

  [ Nelson A. de Oliveira ]
  * Added watch file.

  [ Andreas Tille ]
  * Added myself to uploaders
  * Added path to dh_installman
  * Moved additional shellscript wrapper boxshade.sh to debian dir
  * Added debian/get-orig-source script and added get-orig-source
    target to debian/rules

 -- Andreas Tille <tille@debian.org>  Tue, 26 Feb 2008 07:28:42 +0100

boxshade (3.3.1-2) unstable; urgency=low

  [Charles Plessy]

  * Debian Menu transition: Apps/Science becomes Applications/Science/Biology.
  * makefile.unx : changes managed using the `quilt' patch system.
  * makefile.unx : commenting `LDFLAGS= -s' so that stripping is handled by
    dh_strip (Closes: #436605).
  * debian/rules : `ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))' test
    removed because dh_strip can work without.

  [Steffen Moeller]

  * Updated README.Debian with reference to Debian-Med
  * Updated email address to moeller@debian.org

 -- Charles Plessy <charles-debian-nospam@plessy.org>  Thu,  9 Aug 2007 10:38:31 +0900

boxshade (3.3.1-1) unstable; urgency=low

  [ Steffen Moeller ]
  * Removed [biology] tag from description

  [ Charles Plessy ]
  * New version number, but upsteams' 3.3.1 was already packaged in Debian
    under number 3.1.1 (Closes: #385255).
  * Swiching to collaborative maintainance: Maintainer is a mailing-list,
    Uploaders are real people.
  * Tidying the package:
    - Using debhelper v5 and conforming to policy 3.7.2.
    - Updated manpage using the template of docbook-xsl 1.71.0.dfsg.1-1.1.
    - Converted the manpage to unicode.
    - Relicenced the manpage to "same licence as Boxshade itself", with the
      agreement of Steffen.
    - Added XS-Vcs-Svn and XS-Vcs-Browser to debian/control.
    - Removed empty files debian/dirs, debian/install, debian/conffiles.
  * Replaced clustalw (non-freee) by kalign (free)|clustalw in the Suggests field.
  * Changed priority to optional.

 -- Charles Plessy <charles-debian-nospam@plessy.org>  Sun, 25 Feb 2007 08:47:18 +0900

boxshade (3.1.1-1) unstable; urgency=low

  * Initial Release (Closes: #234923).

 -- Steffen Moeller <moeller@pzr.uni-rostock.de>  Thu, 26 Feb 2004 13:17:22 +0100
